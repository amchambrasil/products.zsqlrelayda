# -*- coding: UTF-8 -*-

database_type = 'SQLRelay'

import os
import os.path
from db import DB
from thread import allocate_lock

from Shared.DC import ZRDB
import DABase
import Shared.DC.ZRDB.Connection
from App.ImageFile import ImageFile
import Globals

manage_addZSQLRelayConnectionForm = Globals.HTMLFile('connectionAdd', globals())


def manage_addZSQLRelayConnection(self, id, title,
                                connection_string,
                                check=None,
                                REQUEST=None):
    """Add a DB connection to a folder"""
    self._setObject(id,
           Connection(id, title, connection_string, check))
    if REQUEST is not None:
        return self.manage_main(self, REQUEST)

_Connection = Shared.DC.ZRDB.Connection.Connection
_connections = {}
_connections_lock = allocate_lock()


class Connection(DABase.Connection):
    """ ZSQLRelay Database Adapter Connection.
    """
    database_type = database_type
    id = '%s_database_connection' % database_type
    meta_type = title = 'Z %s Database Connection' % database_type
    icon = 'misc_/Z%sDA/conn' % database_type

    _v_connected = ''

    def factory(self):
        """ Base API. Returns factory method for DB connections.
        """
        return DB

    def table_info(self):
        return self._v_database_connection.table_info()


classes = ('DA.Connection',)

meta_types = (
    {'name': 'Z %s Database Connection' % database_type,
     'action': 'manage_addZ%sConnectionForm' % database_type,
     },
)

folder_methods = {
    'manage_addZSQLRelayConnection':
    manage_addZSQLRelayConnection,
    'manage_addZSQLRelayConnectionForm':
    manage_addZSQLRelayConnectionForm,
}

__ac_permissions__ = (
    ('Add Z SQLRelay Database Connections',
     ('manage_addZSQLRelayConnectionForm',
      'manage_addZSQLRelayConnection')),
)

misc_ = {'conn': ImageFile(
    os.path.join(
        os.path.dirname(ZRDB.__file__), 'www', 'DBAdapterFolder_icon.gif')
)}

for icon in ('table', 'view', 'stable', 'what',
        'field', 'text', 'bin', 'int', 'float',
        'date', 'time', 'datetime'):
    misc_[icon] = ImageFile(os.path.join('icons', '%s.gif') % icon, globals())
