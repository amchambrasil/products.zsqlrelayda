# -*- coding: utf-8 -*-

from SQLRelay import PySQLRDB, PySQLRClient
from Shared.DC.ZRDB.TM import TM

import string, sys
from string import strip, split
from time import time
from coig_utils import get_ora_out_conv

failures=0
calls=0
last_call_time=time()


class DB(TM):

    Database_Error = PySQLRDB.DatabaseError
    Database_Connection=PySQLRDB.connect

    def __init__(self,connection):
        info = string.split(connection)
        if len(info)==5:
            self.host = info[0]
            self.port = int(info[1])
            self.socket = info[2]
            self.user = info[3]
            self.password = info[4]
        elif len(info)==4:
            self.host = info[0]
            self.port = int(info[1])
            self.socket = ""
            self.user = info[2]
            self.password = info[3]
        elif len(info)==3:
            self.host = ""
            self.port = 0
            self.socket = info[0]
            self.user = info[1]
            self.password = info[2]
        else:
            raise self.Database_Error, ('Invalid connection string, <code>%s</code>')

    def str(self,v, StringType=type('')):
        if v is None: return ''
        r=str(v)
        if r[-1:]=='L' and type(v) is not StringType: r=r[:-1]
        return r

    def _begin(self):
        self.con = PySQLRDB.connect(self.host, self.port, self.socket, self.user, self.password,0,1)
        #self.cur = self.con.cursor()
        self.cur = PySQLRClient.sqlrcursor(self.con)
        self.cur.getNullsAsNone()
        self.cur.setResultSetBufferSize(10)

    def _finish(self, *ignored):
        self.con.commit()
        self.con.close()

    def _abort(self, *ignored):
        self.con.rollback()
        self.con.close()

    def tables(self, rdb=0, _care=('TABLE', 'VIEW')):
        r=[]
        a=r.append
        for name, typ in self.db.objects():
            if typ in _care:
                a({'TABLE_NAME': name, 'TABLE_TYPE': typ})
        return r

    def columns(self, table_name):
        try:
            r=self.cur.execute('select * from %s' % table_name)
        except:
            return ()
        desc=self.cur.description
        r=[]
        a=r.append
        for name, type, width, ds, p, scale, null_ok in desc:
            if type=='NUMBER' and scale==0: type='INTEGER'
            a({ 'Name': name,
                'Type': type,
                'Precision': p,
                'Scale': scale,
                'Nullable': null_ok,
                })
        return r

    def query(self, query_string, max_rows=9999999):
        global failures, calls, last_call_time
        calls=calls+1
        desc=None
        result=[]
        self._register()

        conversion_list = []
        items=[]

        try:
            for qs in filter(None, map(strip,split(query_string, '\0'))):
                r=self.cur.sendQuery(qs)  # 1 jesli OK, 0 jesli blad
                #self.con.endSession()
                if r:
                    for i in range(0,self.cur.colCount()):
                        name      = self.cur.getColumnName(i)
                        type      = self.cur.getColumnType(i)
                        length    = self.cur.getColumnLength(i)
                        precision = self.cur.getColumnPrecision(i)
                        scale     = self.cur.getColumnScale(i)

                        if type=='NUMBER':
                            typec='n'
                        elif type=='DATE':
                            typec='d'
                        else: typec='s'
                        item = {
                            'name': name,
                            'type': typec,
                            'sqltype':type,
                            'precision':precision,
                            'scale':scale}
                        items.append( item )
                        conversion_list.append( get_ora_out_conv( item ) )


                    #fl = open('db_py.log', 'a+')
                    #fl.write('qs: %s\n\r' % (qs))
                    done  = 0
                    rowNo = 0
                    while (not done) and rowNo<max_rows:
                        #fl.write('rowNo: %s\n\r' % (rowNo))
                        #for col in range(self.cur.colCount()):
                        row = self.cur.getRow(rowNo)
                        #fl.write('row: %s\n\r' % (row))
                        if row:
                            row_casted = map(lambda x: x[0](x[1]), zip(conversion_list, row))
                            result.append(row_casted)
                        else:
                            done=1
                        rowNo+=1
                    #fl.close()

                    """r=self.cur.execute(qs)
                if r is None:
                    if desc is not None:
                        if self.cur.description != desc:
                            raise 'Query Error', (
                                'Multiple select schema are not allowed'
                                )
                        if 'append' not in dir(result):
                            result = list(result)
                        if max_rows:
                            for row in self.cur.fetchmany(max_rows-len(result)):
                                result.append(row)
                    else:
                        desc=self.cur.description
                        if max_rows:
                            if max_rows==1: result=(self.cur.fetchone(),)
                            else: result=self.cur.fetchmany(max_rows)
                    """
                else:
                    raise self.Database_Error( self.cur.errorMessage() )
            failures=0
            last_call_time=time()
        except self.Database_Error, mess:
            failures=failures+1
            if (failures > 1000 or time()-last_call_time > 600):
                # Hm. maybe the db is hosed.  Let's try again.
                failures=0
                last_call_time=time()
                return self.query(query_string, max_rows)
            else: raise sys.exc_type, sys.exc_value, sys.exc_traceback

        #if desc is None:
        #    return (),()
        return items, result