import DA

misc_ = DA.misc_


def initialize(context):

    context.registerClass(
        DA.Connection,
        permission = 'Add Z SQLRelay Database Connections',
        constructors = (DA.manage_addZSQLRelayConnectionForm,
                       DA.manage_addZSQLRelayConnection),
    )
